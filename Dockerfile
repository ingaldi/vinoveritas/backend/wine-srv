FROM maven:3.6.0-jdk-8-alpine AS build
WORKDIR /usr/src/app
 

COPY pom.xml . 
RUN mvn dependency:go-offline

COPY src/ ./src/
RUN ls  src -lR

RUN mvn package -Dmaven.test.skip=true

FROM  openjdk:8-jdk-alpine AS bake
COPY --from=build usr/src/app/target/winesrv-0.1.0.jar /usr/bin/winesrv-0.1.0.jar
EXPOSE 8080  
CMD java -jar /usr/bin/winesrv-0.1.0.jar 
