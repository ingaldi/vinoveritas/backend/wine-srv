# Wine-Srv

Wine-Srv is the microservice that encapsulates the "wine" aggregate. Enable creation, modification and queries over the wines in the warehouse and all related data.

## Resources

	- Ports: 8080 (http)
	
## Run (dev)
after cloning the repository, run ./run.sh

## Build
Wine-Srv is built with a docker image, so use

cd <your-folder>
docker builld -t vinoveritas/wine-srv .
