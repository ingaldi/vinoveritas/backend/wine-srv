package com.vinoveritas.winesrv.application.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HealthController {

    @RequestMapping("/health")
    public ResponseEntity<?> health()
    {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}