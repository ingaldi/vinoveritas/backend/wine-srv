package com.vinoveritas.winesrv.application.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vinoveritas.winesrv.application.dtos.WineAmountDTO;
import com.vinoveritas.winesrv.application.dtos.WineDTO;
import com.vinoveritas.winesrv.core.factories.Winefactory;
import com.vinoveritas.winesrv.core.models.Wine;
import com.vinoveritas.winesrv.core.repositories.WineRepository;

@RestController
@RequestMapping(value = "/wines")
public class WineController 
{
	@Autowired
	WineRepository wineRepo;
	

	@PostMapping
	public ResponseEntity<Map<String, String>> addWine (@Valid @RequestBody WineDTO reqBody)
	{
		Wine toInsert = new Winefactory(wineRepo).getInstance(reqBody.getName(), reqBody.getAmount());

		
		Map<String, String> respBody = new HashMap<String,String>();
		respBody.put("id", toInsert.getId().toString());

		
		return new ResponseEntity<Map<String, String>>(respBody , HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}" , method = RequestMethod.PATCH  , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateWineProperties(@PathVariable("id") int id , @Valid @RequestBody WineAmountDTO amount)
	{
		Optional<Wine> output = Optional.empty();
			
		output = wineRepo.updateProperty(id, "amount", amount.getAmount());
		
		System.out.println(" wine id: " + output.get().getId());

		
		Map<String, String> respBody = new HashMap<String,String>();
		ResponseEntity<Map<String, String>>  resp = new ResponseEntity<>(respBody , HttpStatus.NOT_FOUND);
		

		if (output.isPresent())
		{
			Wine wine = output.get();
			respBody.put("name", wine.getName());
			respBody.put("amount" , wine.getAmount().toString());
			respBody.put("id", wine.getId().toString());
			
			resp = new ResponseEntity<Map<String, String> >(respBody , HttpStatus.OK);
		}
	
		return resp;
	}
	
	
	@RequestMapping(value="/{id}" , method = RequestMethod.GET  , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getWineById(@PathVariable("id") int id)
	{
		Optional<Wine> output = Optional.empty();
		
		output = wineRepo.findById(id);

		
		Map<String, String> respBody = new HashMap<String,String>();
		ResponseEntity<Map<String, String>>  resp = new ResponseEntity<>(respBody , HttpStatus.NOT_FOUND);
		

		if (output.isPresent())
		{
			Wine wine = output.get();
			respBody.put("name", wine.getName());
			respBody.put("amount" , wine.getAmount().toString());
			respBody.put("id", wine.getId().toString());
			
			resp = new ResponseEntity<Map<String, String> >(respBody , HttpStatus.OK);
		}
	
		return resp;
	}
	

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Wine>> getWineAll()
	{
		
		List<Wine> allWines = wineRepo.findAll();
		
		return new ResponseEntity<List<Wine>>(allWines , HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}" , method = RequestMethod.DELETE  , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteWineById(@PathVariable("id") int id)
	{
		
		HttpStatus status = wineRepo.deleteById(id) ? HttpStatus.NO_CONTENT : HttpStatus.NOT_FOUND;
	
		return new ResponseEntity<>(status);
	}

    @ExceptionHandler
    public ResponseEntity<?> handleException(MethodArgumentNotValidException exception) {
 
 
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }

}
