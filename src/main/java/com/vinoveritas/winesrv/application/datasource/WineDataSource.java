package com.vinoveritas.winesrv.application.datasource;


import java.util.HashMap;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
public class WineDataSource extends HashMap<Integer , WineDataSource.Item>
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static int uuid = 0;

	public static class Item
	{
		private String name;
		private int amount;
		private int id;
		
		public Item(String name, int amount)
		{
			super();
			
			this.id = uuid++;
			this.name = name;
			this.amount = amount;
		}
		
		public Item(int id , String name, int amount)
		{
			super();
			
			this.id = id;
			this.name = name;
			this.amount = amount;
		}
				

		public int getId() {
			return id;
		}



		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAmount() {
			return amount;
		}

		public void setAmount(int amount) {
			this.amount = amount;
		}

		
	}
}
