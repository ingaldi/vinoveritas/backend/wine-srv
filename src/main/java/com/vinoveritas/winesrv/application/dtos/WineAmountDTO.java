package com.vinoveritas.winesrv.application.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

public class WineAmountDTO
{	
	private Integer amount;

	@NotNull
	@Min(value=0)
	public WineAmountDTO() {
		super();
	}

	public WineAmountDTO(Integer amount) {
		super();
		this.amount = amount;
	}

	public Integer getAmount() {
		return amount;
	}
	
	
}
