package com.vinoveritas.winesrv.application.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class WineDTO 
{
	@NotNull
	private String name ;
	@NotNull
	@Min(value=0)
	private Integer amount;
	
	
	public WineDTO() 
	{
		this.name = "";
		this.amount = 0;
	}

	public WineDTO(String name, Integer amount)
	{
		this.name = name;
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public Integer getAmount() {
		return amount;
	}
	
	
}
