package com.vinoveritas.winesrv.core.factories;

import com.vinoveritas.winesrv.core.models.Wine;
import com.vinoveritas.winesrv.core.repositories.WineRepository;

public class Winefactory
{
	private WineRepository repo;
	
	
	public Winefactory(WineRepository repo) {
		super();
		this.repo = repo;
	}


	public Wine getInstance(String name , Integer amount)
	{
		Wine partialOutput = new Wine(-1 , name , amount);

		Integer id = repo.save(partialOutput);
		
		return new Wine(id , partialOutput.getName() , partialOutput.getAmount());
	}
}
