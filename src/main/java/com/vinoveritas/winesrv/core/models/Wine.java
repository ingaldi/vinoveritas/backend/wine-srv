package com.vinoveritas.winesrv.core.models;

public class Wine 
{
	private Integer id;
	private String name;
	private Integer amount;
	
	
	public Wine()
	{
		super();
		this.name = "";
		this.amount = 0;
	}	
	
	public Wine(Integer id, String name, Integer amount)
	{
		super();
		this.id = id;
		this.name = name;
		this.amount = amount;
	}
	
	
	
	public Integer getId() {
		return id;
	}
	
	

	public String getName() {
		return name;
	}

	public Integer getAmount() {
		return amount;
	}
	
}
