package com.vinoveritas.winesrv.core.repositories;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vinoveritas.winesrv.application.datasource.WineDataSource;
import com.vinoveritas.winesrv.core.models.Wine;

@Repository
public class WineRepository
{
	@Autowired
	private WineDataSource cache;
	
	
	public Integer save(Wine wine)
	{
		WineDataSource.Item item = new WineDataSource.Item(wine.getName() ,wine.getAmount());
		cache.put(item.getId(), item);
		
		return item.getId();
	}
	
	public Optional<Wine> updateProperty(Integer id , String property , Object value)
	{
		cache.put(99 , new WineDataSource.Item(99 , "morellino" , 10));
		Wine out = null;
		
		if (cache.containsKey(id) && property == "amount")
		{
			cache.get(id).setAmount((Integer)value);
			WineDataSource.Item item = cache.get(id);
			
			out = new Wine(item.getId() , item.getName() , item.getAmount());

		}
		
		return Optional.of(out);
		
	}
	
	public Optional<Wine> findById (Integer id)
	{
		Optional<Wine> output = Optional.empty();
		
		if (cache.containsKey(id))
		{
			WineDataSource.Item item = cache.get(id);
			return Optional.of(new Wine (item.getId() , item.getName() , item.getAmount() ));
		}
		
		return output;
	}
	
	public List<Wine> findAll ()
	{
		return cache.values().stream().map(WineRepository::mapWine).collect(Collectors.toList());
	}
	
	private static Wine mapWine (WineDataSource.Item item)
	{
		return new Wine (item.getId() , item.getName() , item.getAmount());
	}
	
	public boolean deleteById (Integer id)
	{
		if (!cache.containsKey(id))
		{
			return false;
		}
		
		cache.remove(id);
		return true;
	}
}
