package com.vinoveritas.winesrv.controllers;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.vinoveritas.winesrv.application.datasource.WineDataSource;

@Profile("WineControllerTest")
@Configuration
public class WineControllerTestConfig 
{
    @Bean
    @Primary
    public WineDataSource wineDataSource() {
        return Mockito.mock(WineDataSource.class);
    }
}