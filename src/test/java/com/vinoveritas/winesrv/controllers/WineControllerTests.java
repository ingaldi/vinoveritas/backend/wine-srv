package com.vinoveritas.winesrv.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.vinoveritas.winesrv.application.controllers.WineController;
import com.vinoveritas.winesrv.application.datasource.WineDataSource;
import com.vinoveritas.winesrv.core.models.Wine;
import com.vinoveritas.winesrv.core.repositories.WineRepository;

@ActiveProfiles("WineControllerTests")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WineControllerTests {

    @Autowired
    private MockMvc mockMvc;
    
    @Spy
    private WineDataSource dataSource;
    
    @Before
    public void setUp ()
    {
        dataSource.put(99 , new WineDataSource.Item(99 , "morellino" , 10));
    }

    @Test
    public void shouldCreateAWine() throws Exception 
    {

        this.mockMvc
        .perform(post("/wines").contentType(MediaType.APPLICATION_JSON).content("{\"name\" : \"negramaro\" , \"amount\":1}"))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").value("0"));      
    }
    
    
    @Test
    public void shouldRejectIfNoWineNameProvided() throws Exception 
    {

        this.mockMvc
        .perform(post("/wines").contentType(MediaType.APPLICATION_JSON).content("\"amount\":1}"))
        .andExpect(status().is(400));
    }
    
    @Test
    public void shouldRejectIfNegativeAmountProvided() throws Exception 
    {

        this.mockMvc
        .perform(post("/wines").contentType(MediaType.APPLICATION_JSON).content("\"amount\": -1}"))
        .andExpect(status().is(400));
    }
    

    @Test
    public void shouldChangeAmount() throws Exception 
    {
        this.mockMvc
        .perform(patch("/wines/99").contentType(MediaType.APPLICATION_JSON).content("{\"amount\":1}"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.amount").value("1"));      
    }

    
    @Test
    public void shouldBe404IfNoIdIsPresent() throws Exception 
    {
        this.mockMvc
        .perform(patch("/wines/1000").contentType(MediaType.APPLICATION_JSON).content("{\"amount\":1}"))
        .andExpect(status().isNotFound());
    }
    

}
